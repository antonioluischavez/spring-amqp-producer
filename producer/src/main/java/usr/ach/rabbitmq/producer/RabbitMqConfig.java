package usr.ach.rabbitmq.producer;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import usr.ach.rabbitmq.producer.serialize.ExecutionReportSerializer;

@Configuration
public class RabbitMqConfig {

  static final String EXCHANGE_NAME = "spring-boot-exchange";

  private static final String QUEUE_NAME = "spring-boot-queue";

  static final String BINDING_KEY = "spring-boot-binding-key";

  @Bean
  Queue queue() {
    return new Queue(QUEUE_NAME, false);
  }

  @Bean
  TopicExchange exchange() {
    return new TopicExchange(EXCHANGE_NAME);
  }

  @Bean
  Binding binding(final Queue queue, final Exchange exchange) {
    return BindingBuilder.bind(queue).to(exchange).with(BINDING_KEY).noargs();
  }

  @Bean
  MessageConverter messageConverter() {
    return new ProtobufMessageConverter<>(new ExecutionReportSerializer());
  }
}
