package usr.ach.rabbitmq.producer.serialize;

public interface ProtobufSerializer<T> {

    byte[] serialize(T object);
    T deserialize(byte[] message);
}