package usr.ach.rabbitmq.producer.serialize;

import com.google.protobuf.InvalidProtocolBufferException;
import usr.ach.rabbitmq.producer.model.*;
import usr.ach.rabbitmq.producer.protobuf.ExecutionReportProto;

public class ExecutionReportSerializer implements ProtobufSerializer<ExecutionReport> {
    @Override
    public byte[] serialize(ExecutionReport object) {
        return ExecutionReportProto.ExecutionReport.newBuilder()
                .setBeginString(object.getBeginString())
                .setMessageLength(object.getMessageLength())
                .setMessageType(ExecutionReportProto.ExecutionReport.MessageType.valueOf(object.getMessageType().name()))
                .setMessageSequenceNum(object.getMessageSequenceNum())
                .setSenderCompID(object.getSenderCompID())
                .setTargetCompID(object.getTargetCompID())
                .setSendingTime(object.getSendingTime())
                .setAvgPx(object.getAvgPx())
                .setOrderQty(object.getOrderQty())
                .setCumQty(object.getCumQty())
                .setLeavesQty(object.getLeavesQty())
                .setLastQty(object.getLastQty())
                .setLastPx(object.getLastPx())
                .setExecType(ExecutionReportProto.ExecutionReport.ExecType.valueOf(object.getExecType().name()))
                .setExecID(object.getExecID())
                .setOrdStatus(ExecutionReportProto.ExecutionReport.OrderStatus.valueOf(object.getOrdStatus().name()))
                .setOrderId(object.getOrderId())
                .setOrdType(ExecutionReportProto.ExecutionReport.OrderType.valueOf(object.getOrdType().name()))
                .setSide(ExecutionReportProto.ExecutionReport.OrderSide.valueOf(object.getSide().name()))
                .setSymbol(object.getSymbol())
                .setCurrency(object.getCurrency())
                .setCheckSum(object.getCheckSum())
                .build()
                .toByteArray();
    }

    @Override
    public ExecutionReport deserialize(byte[] message) {
        try {
            ExecutionReportProto.ExecutionReport protoObject = ExecutionReportProto.ExecutionReport.parseFrom(message);
            return ExecutionReport.builder()
                    .beginString(protoObject.getBeginString())
                    .messageLength(protoObject.getMessageLength())
                    .messageType(MessageType.valueOf(protoObject.getMessageType().name()))
                    .messageSequenceNum(protoObject.getMessageSequenceNum())
                    .senderCompID(protoObject.getSenderCompID())
                    .targetCompID(protoObject.getTargetCompID())
                    .sendingTime(protoObject.getSendingTime())
                    .avgPx(protoObject.getAvgPx())
                    .orderQty(protoObject.getOrderQty())
                    .cumQty(protoObject.getCumQty())
                    .leavesQty(protoObject.getLeavesQty())
                    .lastQty(protoObject.getLastQty())
                    .lastPx(protoObject.getLastPx())
                    .execType(ExecType.valueOf(protoObject.getExecType().name()))
                    .execID(protoObject.getExecID())
                    .ordStatus(OrderStatus.valueOf(protoObject.getOrdStatus().name()))
                    .orderId(protoObject.getOrderId())
                    .ordType(OrderType.valueOf(protoObject.getOrdType().name()))
                    .side(OrderSide.valueOf(protoObject.getSide().name()))
                    .symbol(protoObject.getSymbol())
                    .currency(protoObject.getCurrency())
                    .checkSum(protoObject.getCheckSum())
                    .build();
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
