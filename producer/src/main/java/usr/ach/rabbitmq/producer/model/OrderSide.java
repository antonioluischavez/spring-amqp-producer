package usr.ach.rabbitmq.producer.model;

public enum OrderSide {
    BUY,
    SELL,
    BUYMINUS,
    SELLPLUS,
    SELLSHORT,
    SELLSHORTEXEMPT,
    UNDISCLOSED,
    CROSS,
    CROSSSHORT,
    ASDEFINED,
    OPPOSITE
}
