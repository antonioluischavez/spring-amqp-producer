package usr.ach.rabbitmq.producer;

import static usr.ach.rabbitmq.producer.RabbitMqConfig.EXCHANGE_NAME;
import static usr.ach.rabbitmq.producer.RabbitMqConfig.BINDING_KEY;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import usr.ach.rabbitmq.producer.model.*;

@Component
public class ScheduledTask {

  private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTask.class);

  private final RabbitTemplate template;

  private Long execId;

  public ScheduledTask(RabbitTemplate template) {
    this.template = template;
    execId = 0L;
  }

  @Scheduled(fixedRate = 10000)
  public void sendMessage() {
    final ExecutionReport report = generateMessage();
    template.convertAndSend(EXCHANGE_NAME, BINDING_KEY, report);
    increaseExecID();
  }

  private synchronized void increaseExecID() {
        this.execId++;
  }

  private ExecutionReport generateMessage() {
    return ExecutionReport.builder()
        .beginString("FIX.4.4.")
        .messageLength(289)
        .messageType(MessageType.EXECUTIONREPORT)
        .messageSequenceNum(1090)
        .senderCompID("TESTSELL1")
        .targetCompID("TA001")
        .sendingTime("20180920-18:23:53.671")
        .avgPx(113.35f)
        .orderQty(7000)
        .cumQty(3500)
        .leavesQty(3500)
        .lastQty(3500)
        .lastPx(113.35f)
        .execType(ExecType.EXECTYPE_TRADE)
        .execID(execId.toString())
        .ordStatus(OrderStatus.ORDSTATUS_PARTIALLYFILLED)
        .orderId("20636730646335310000")
        .ordType(OrderType.MARKET)
        .side(OrderSide.BUY)
        .symbol("MSFT")
        .currency("USD")
        .checkSum("151")
        .build();
  }
}
