package usr.ach.rabbitmq.producer.model;

public enum MessageType {
    HEARTBEAT,
    TESTREQUEST,
    RESENDREQUEST,
    REJECT,
    SEQUENCERESET,
    LOGOUT,
    EXECUTIONREPORT,
    ORDERCANCELREJECT,
    LOGON,
    NEWS,
}
