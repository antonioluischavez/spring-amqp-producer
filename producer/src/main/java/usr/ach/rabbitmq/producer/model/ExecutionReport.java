package usr.ach.rabbitmq.producer.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ExecutionReport {

    private String beginString;

    private int messageLength;

    private MessageType messageType;

    private int messageSequenceNum;

    private String senderCompID;

    private String targetCompID;

    private String sendingTime;

    private float avgPx;

    private int orderQty;

    private int cumQty;

    private int leavesQty;

    private int lastQty;

    private float lastPx;

    private ExecType execType;

    private String execID;

    private OrderStatus ordStatus;

    private String orderId;

    private OrderType ordType;

    private OrderSide side;

    private String symbol;

    private String currency;

    private String checkSum;
}
